import { AfterViewInit, Component, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CashDataService } from '../cash-data.service';
import { RequestHandlerService } from '../request-handler.service';
import { encode } from 'js-base64';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit, OnDestroy, OnChanges {

  myTestVar1: string = "";
  myTestVarArray1: any[] = [];
  myTestVar2: number = 0;

  email: string = "";
  password: string = "";
  lastName: string = "";
  firstName: string = "";

  isLogin: boolean = true;

  erroMessage: string = "";

  // cashDataService = new CashDataService();

  constructor(private cashDataService: CashDataService, private router: Router, private requestHandlerService: RequestHandlerService) {
    console.log("This is from constructor*******");
  }


  ngOnInit(): void {
    console.log("This is from ngOnInit*******");
  }

  ngAfterViewInit() {
    console.log("This is from ngAfterViewInit*******");
  }

  ngOnDestroy() {
    console.log("This is from ngOnDestroy*******");
  }

  ngOnChanges() {
    console.log("This is from ngOnChanges*******");
  }




  getEmployeeName() {
    return "This is employee name";
  }

  getEmail(eventValue: any) {
    this.email = eventValue.target.value;
    this.cashDataService.userName = eventValue.target.value;
  }



  navigate() {

    let bodyData = {
      "email": this.email,
      "password": this.password
    };

    this.requestHandlerService.postRequest("user/login", bodyData).subscribe(  (resultData: any) => {
      console.log(resultData);

      if (resultData.status) {

        localStorage.setItem('token',resultData.token);

        
        let data: any = {
          logged: true
        }
        let encodedData = encode(JSON.stringify(data));
        localStorage.setItem('session-data', encodedData);

        this.router.navigate(['/landing']);
      } else {
        let data: any = {
          logged: false
        }
        let encodedData = encode(JSON.stringify(data));
        localStorage.setItem('session-data', encodedData);
        console.log("Errror login");
      }
    },   (errorData: any) => {
      let data: any = {
        logged: false
      }
      let encodedData = encode(JSON.stringify(data));
      localStorage.setItem('session-data', encodedData);
      console.log(errorData);
      console.log(errorData.error.msg);
      this.erroMessage = errorData.error.msg;
    });


    // this.router.navigate(['/landing']);

  }




  goToRegister() {
    this.isLogin = false;
  }

  goToLogin() {
    this.isLogin = true;
  }


  sendRegisterRequest() {

    let bodyData = {
      "first_name": this.firstName,
      "last_name": this.lastName,
      "email": this.email,
      "password": this.password,
      "role": "user"
    };

    // this.http.post("http://64.227.28.237:5000/user/new", bodyData)

    this.requestHandlerService.postRequest("user/new", bodyData).subscribe((resultData: any) => {
      console.log(resultData);
      this.isLogin = true;
    });


  }
}
