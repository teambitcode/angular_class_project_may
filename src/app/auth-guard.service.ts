import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { decode } from 'js-base64';
@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private router: Router) { }

  canActivate(): boolean {
    let session_data_string_encoded: any = localStorage.getItem('session-data');

    let session_data_string_decoded = decode(session_data_string_encoded);

    let session_data_json: any = JSON.parse(session_data_string_decoded);
    

    if ( session_data_json && session_data_json.logged) {
      return true;
    } else {
      this.router.navigateByUrl("login");
      return false;
    }

  }
}
