import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CashDataService } from '../cash-data.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {


  constructor(public cashDataService: CashDataService, private router: Router) { }

  ngOnInit(): void {
    console.log(this.cashDataService.userName);

    let total = this.cashDataService.getTotalValue(100, 25);
    console.log(total);
  }

  logOut(){
    localStorage.removeItem('session-data');
    this.router.navigateByUrl("login");
  }
}
