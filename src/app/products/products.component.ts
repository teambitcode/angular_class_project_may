import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RequestHandlerService } from '../request-handler.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  filterText: string = '';
  myAge: any = 0;
  myAge2: number = 0;

  total: number = 0;
  productsArray: any[] = [];
  isResultLoded = false;
  isUpdateFormActive = false;

  formProductName = "";
  formProductPrice = "";
  formProductDescription = "";
  currentProductId = "";

  // productsArray: any[] = [
  //   {
  //     'name': 'laptop 2X',
  //     'qty': 120,
  //     'price': 200000
  //   },
  //   {
  //     'name': 'ffdsf fdhgh df',
  //     'qty': 30,
  //     'price': 10000
  //   },
  //   {
  //     'name': 'aaa sf fff',
  //     'qty': 400,
  //     'price': 600
  //   },
  //   {
  //     'name': 'hhhh wwer',
  //     'qty': 200,
  //     'price': 200000
  //   },
  //   {
  //     'name': 'laptop 900X',
  //     'qty': 3070,
  //     'price': 5000
  //   },
  //   {
  //     'name': 'hhh gh',
  //     'qty': 40,
  //     'price': 7000
  //   },
  //   {
  //     'name': 'aaa fg bgn',
  //     'qty': 800,
  //     'price': 1000
  //   }

  // ];

  constructor(private requestHandlerService: RequestHandlerService) { }

  ngOnInit(): void {
    this.getAllProducts();
  }

  getAllProducts() {

    // this.http.get("http://64.227.28.237:5000/product/getAll")
    
    this.requestHandlerService.getRequest("product/getAll").subscribe((resultData: any) => {
      console.log(resultData);
      this.isResultLoded = true;
      this.productsArray = resultData.data;
    });

    let myVar = 10;
    let result = myVar +100;
    console.log(result);

  }


  getPriceWithCurrency(price: any) {

    console.log("getPriceWithCurrency methid called");
    return 'LKR ' + price;

  }


  getProductsArray() {
    console.log("getProductsArray called...!");
    let resultArray = [];

    for (let index = 0; index < this.productsArray.length; index++) {
      let element = this.productsArray[index];
      if (element.name.indexOf(this.filterText) != -1) {
        resultArray.push(element);
      }
    }

    return resultArray;

  }

  setFilterText(eventData: any) {
    this.filterText = eventData.target.value;
  }

  getTotal(eventData: any) {
    this.total = parseInt(eventData.data) + parseInt(this.myAge);
    console.log(this.total);
  }

  setUpdateFormData(data: any){
    console.log(data);
    this.formProductName = data.name;
    this.formProductDescription = data.description;
    this.formProductPrice = data.price;
    this.currentProductId = data._id;

    this.isUpdateFormActive = true;
  }

  sendUpdateRequest(){

    let body = {
      "name": this.formProductName,
      "price": parseFloat(this.formProductPrice),
      "description": this.formProductDescription
    }

    this.requestHandlerService.updateRequest("product/update", body, this.currentProductId ).subscribe((resultData: any) => {
      console.log(resultData);
      Swal.fire(
        'Updated!',
        'Your product has been update.',
        'success'
      );
      this.getAllProducts();
    });


  }


  sendDeleteRequest(data: any){

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.requestHandlerService.deleteRequest("product/remove", data._id ).subscribe((resultData: any) => {
          console.log(resultData);
          Swal.fire(
            'Deleted!',
            'Your product has been deleted.',
            'success'
          );
          this.getAllProducts();
        });
  
      }
    })
  


  }

}
