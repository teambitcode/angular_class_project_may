import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-test-event',
  templateUrl: './test-event.component.html',
  styleUrls: ['./test-event.component.scss']
})
export class TestEventComponent implements OnInit, OnChanges {

  @Input() age: number = 0;

  totle: number =0;
  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges){
    console.log(changes.age.currentValue);

    console.log("ngOnChanges called inside TestEventComponent *********");
  }

}
