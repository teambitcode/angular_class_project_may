import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './auth-guard.service';
import { LandingComponent } from './landing/landing.component';
import { LoginComponent } from './login/login.component';
import { ProductsComponent } from './products/products.component';
import { TableComponent } from './table/table.component';

let landingChildRoutes: Routes = [
  {
    path:'products',
    component: ProductsComponent
  },
  {
    path:'abc',
    component: LoginComponent
  }
];





let routes: Routes = [
  {
    path:'login',
    component: LoginComponent
  },
  {
    path:'landing',
    component: LandingComponent,
    children: landingChildRoutes,
    canActivate: [AuthGuardService]
  }
];




@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
