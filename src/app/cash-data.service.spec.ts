import { TestBed } from '@angular/core/testing';

import { CashDataService } from './cash-data.service';

describe('CashDataService', () => {
  let service: CashDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CashDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
