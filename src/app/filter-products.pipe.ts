import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterProducts'
})
export class FilterProductsPipe implements PipeTransform {

   transform(value:any[], filterText:any): any[] {

    console.log("filterProducts pipe called...!");
    let resultArray = [];

    for (let index = 0; index < value.length; index++) {
      let element = value[index];
      if(element.name.indexOf(filterText) != -1){
        resultArray.push(element);
      }
    }

    return resultArray;
  }

}
