import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  myAmount: number = 28888;

  testValue:number = 50;

  testDisplay: string = "";

studentsArray : any[] = [
  {
    "first_name": "Saman",
    "last_name": "lastTTTT",
    "age": 23
  },
  {
    "first_name": "Amal",
    "last_name": "lastYYYY",
    "age": 50
  },
  {
    "first_name": "Kamal",
    "last_name": "lastOOOO",
    "age": 70
  },
  {
    "first_name": "Janith",
    "last_name": "lastKKKK",
    "age": 80
  },
  {
    "first_name": "Akila",
    "last_name": "lastWWWW",
    "age": 20
  }
];


  constructor() { }

  ngOnInit(): void {
  }


  myClickFunction(){
    console.log("button clicked...!");
  }

  myTypeFunction(eventData: any){
    console.log(eventData.target.value);
    this.testDisplay = eventData.target.value;
  }


  myDisplay(myEvent: any){
    console.log("This is from <app-table> component");
    console.log(myEvent);
  }

}
