import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-my-text-box',
  templateUrl: './my-text-box.component.html',
  styleUrls: ['./my-text-box.component.scss']
})
export class MyTextBoxComponent implements OnInit {

  
  @Input() textBoxInputVariable: string = "";

  @Output() outMyVar = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  clickOnOutput(){
    console.log("This is from <app-my-text-box> component");
    this.outMyVar.emit("My output event fired....!!");
  }

}
