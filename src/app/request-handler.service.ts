
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RequestHandlerService {

  constructor(private http: HttpClient) { }

  setHeaders(): HttpHeaders {
    let options: any = {
      'Content-Type': 'application/json'
    };

    let tempToken = localStorage.getItem('token');
    
    if(tempToken){
      options['Authorization'] = tempToken;
    }

    let httpOptions: HttpHeaders = new HttpHeaders(options);

    return httpOptions;

  }

  getRequest(endpoint: string) {

    let port = environment.port;
    let ip = environment.ip;

    return this.http.get("http://" + ip + ":" + port + "/" + endpoint, { headers: this.setHeaders() });

  }

  postRequest(endpoint: string, body: any) {

    let port = environment.port;
    let ip = environment.ip;
                        //  http://64.227.28.237:5000/user/new
    return this.http.post("http://" + ip + ":" + port + "/" + endpoint,body, { headers: this.setHeaders() });

  }


  updateRequest(endpoint: string, body: any, id: string) {

    let port = environment.port;
    let ip = environment.ip;

    return this.http.patch("http://" + ip + ":" + port + "/" + endpoint+ "/"+id , body, { headers: this.setHeaders() });
                      
  }

  deleteRequest(endpoint: string, id: string) {

    let port = environment.port;
    let ip = environment.ip;

    return this.http.delete("http://" + ip + ":" + port + "/" + endpoint+ "/"+id , { headers: this.setHeaders() });
                      
  }

}




//http://64.227.28.237:5000/product/getAll"
// http://"+ip+":"+port+"/"+endpoint?